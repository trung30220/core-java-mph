package jan12;

public class Main2 {
    public static void main(String[] args) {
        char[][] board = {
                {'x', 'o', 'x'},
                {'x', 'x', 'x'},
                {'o', 'o', 'x'}
        };

        checkHorizontal(board, 'x');
        checkHorizontal(board, 'o');
    }

    private static void checkHorizontal(char[][] board, char c) {
        checkHorizontal2(board, c, 0, true);
        checkHorizontal2(board, c, 1, true);
        checkHorizontal2(board, c, 2, true);
        checkHorizontal2(board, c, 0, false);
        checkHorizontal2(board, c, 1, false);
        checkHorizontal2(board, c, 2, false);
    }

    private static void checkHorizontal2(char[][] board, char c, int i, boolean horizontal) {
        if (horizontal) {
            if (board[i][0] == c && board[i][1] == c && board[i][2] == c) {
                System.out.println(c + " is winner horizontal");
            }
        } else {
            if (board[0][i] == c && board[1][i] == c && board[2][i] == c) {
                System.out.println(c + " is winner vertical");
            }
        }
    }
}
