package jan12;

public class Main3 {
    static char[][] board = new char[100][100];
    static int ii = 0, jj = 0;

    public static void main(String[] args) {
        for (int i = 0;i < 4;i++) {
            for (int j = 0;j < 4-i;j++) {
                print(' ');
            }
            for (int j = 0;j < i*2+1;j++) {
                print('*');
            }
            println();
        }

        for (int i = 0;i < 20;i++) {
            for (int j = 0;j < 100;j++) {
                if (board[i][j] == ' ' || board[i][j] == '*') {
                    System.out.print(board[i][j]);
                }
            }
            System.out.println();
        }
    }

    private static void print(char c) {
        board[ii][jj] = c;
        jj++;
    }
    private static void println() {
        ii++;
    }
}
