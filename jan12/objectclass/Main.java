package jan12.objectclass;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int size = 3;
        Student[] arrStudent = new Student[size];

        Scanner scanner = new Scanner(System.in);
        for (int i = 0;i < size;i++) {
            arrStudent[i] = new Student();
            System.out.println(i + ".Nhap ten:");
            arrStudent[i].name = scanner.next();
            arrStudent[i].id = i;
        }

        for (int i = 0;i < size;i++) {
            System.out.println(arrStudent[i]);
        }
    }
}
