package jan12.objectclass;

public class Student {
    long id;
    String name;
    String address;
    float grade;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
