package jan12;

public class Main {
    public static void main(String[] args) {
        int[] arr1 = {3, 4, 9, 11, 1, 2, 4};
        print(arr1);
        for (int i = arr1.length;i > 0;i--) {
            sort(arr1, i); // Arrays.sort(arr1)
        }
        print(arr1);
    }

    private static void sort(int[] arr1, int length) {
        int index = 0;
        int max = arr1[index];
        int from = 1;
        int to = length-1;
        for (int i = from;i <= to;i++) {
            if (arr1[i] > max) {
                max = arr1[i];
                index = i;
            }
        }

        int c = arr1[length-1];
        arr1[length-1] = max;
        arr1[index] = c;

        System.out.println("max = " + max);
    }

    private static void print(int[] arr1) {
        System.out.println("Array:");
        for (int i = 0;i < arr1.length;i++) {
            System.out.print(arr1[i] + ", ");
        }
        System.out.println();
    }
}
