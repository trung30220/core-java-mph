package shoppingcart;

import java.util.ArrayList;
import java.util.List;

public class Main6 { // nhập vô số cart
    public static void main(String[] args) {
        List<Cart> cartList = new ArrayList<>();
        ProductManager.cart.inputManyCart(cartList);
        System.out.println(cartList);

        for (Cart cart : cartList) {
            System.out.println("Thong tin cart ("+cart.createdDate+"):");
            ProductManager.printAll(cart);
            ProductManager.sum(cart);
            System.out.println("\n");
        }
    }
}
