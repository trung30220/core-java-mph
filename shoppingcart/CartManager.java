package shoppingcart;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class CartManager {
    public static void inputManyCart(List<Cart> cartList) {
        System.out.println("Nhap thong tin cart");
        Scanner scanner = new Scanner(System.in);
        while(true) {
            Cart cart = new Cart();
            cart.createdDate = new Date();
            ProductManager.inputManyProduct(cart);
            cartList.add(cart);
            System.out.println("Ban co muon tiep tuc add cart khong (y/n)?:");
            String s = scanner.next();
            if ("n".equalsIgnoreCase(s)) {
                break;
            }
        }
    }
}
