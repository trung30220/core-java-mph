package shoppingcart;

public class Main3 {
    public static void main(String[] args) { // add 3 sản phẩm vào 3 cart có sẵn và tính tổng tiền
        Cart cart1 = new Cart();
        Cart cart2 = new Cart();
        Cart cart3 = new Cart();

        System.out.println("Nhap thong tin san pham");
        Product p1 = ProductManager.inputProduct();
        Product p2 = ProductManager.inputProduct();
        Product p3 = ProductManager.inputProduct();

        ProductManager.addToCart(p1, cart1);
        ProductManager.addToCart(p2, cart1);
        ProductManager.addToCart(p3, cart1);
        ProductManager.addToCart(p1, cart2);
        ProductManager.addToCart(p2, cart2);
        ProductManager.addToCart(p3, cart2);


        // in ra
        ProductManager.printAll(cart1);
        ProductManager.printAll(cart2);

        // tinh tong cart
        ProductManager.sum(cart1);
        ProductManager.sum(cart2);
    }
}
