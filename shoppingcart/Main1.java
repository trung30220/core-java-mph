package shoppingcart;

import java.util.Scanner;

public class Main1 { // add 3 san pham co san vao cart va tinh tong tien
    public static void main(String[] args) {
        Cart cart1 = new Cart();

        System.out.println("Nhap thong tin san pham");
        Product p1 = ProductManager.inputProduct(cart1);
        Product p2 = ProductManager.inputProduct(cart1);
        Product p3 = ProductManager.inputProduct(cart1);

        // in ra
        ProductManager.printAll(cart1);

        // tinh tong cart
        ProductManager.sum(cart1);
    }
}
