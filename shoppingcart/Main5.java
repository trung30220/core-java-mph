package shoppingcart;

import java.util.ArrayList;
import java.util.List;

public class Main5 { // nhập vô số cart
    public static void main(String[] args) {
        List<Cart> cartList = new ArrayList<>();
        ProductManager.inputManyCart(cartList);

        for (Cart cart : cartList) {
            System.out.println("Thong tin cart ("+cart.createdDate+"):");
            ProductManager.printAll(cart);
            ProductManager.sum(cart);
            System.out.println("\n");
        }
    }
}
