package shoppingcart;

import java.util.ArrayList;
import java.util.List;

public class Main0 {
    public static void main(String[] args) {
        Product p1 = new Product(1l, "samsung galaxy", 11000d);

        Product p2 = new Product(2l, "iphone x", 20000d);

        Product p3 = new Product(3l, "mazda", 2000000d);

        Cart2 cart = new Cart2(p1, p2, p3);

        // in ra
        for (Product p : cart.productList) {
            System.out.println(p.toString());
        }

        // tinh tong cart
        double tong = 0d;
        for (Product p : cart.productList) {
            tong += p.price;
        }
        System.out.println("Tong " + tong);
    }
}
