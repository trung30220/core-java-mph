package shoppingcart;

import java.util.Scanner;

public class Main2 { // add vô số sản phẩm vào cart và tính tổng tiền
    public static void main(String[] args) {
        Cart cart1 = new Cart();

        System.out.println("Nhap thong tin san pham");
        Scanner scanner = new Scanner(System.in);
        while(true) {
            Product product = ProductManager.inputProduct(cart1);
            System.out.println("Ban co muon tiep tuc khong (y/n)?:");
            String s = scanner.next();
            if ("n".equalsIgnoreCase(s)) {
                break;
            }
        }

        // in ra
        ProductManager.printAll(cart1);

        // tinh tong cart
        ProductManager.sum(cart1);
    }
}
