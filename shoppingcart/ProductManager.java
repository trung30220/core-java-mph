package shoppingcart;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ProductManager {
    public static Product inputProduct(Cart cart) {
        Scanner scanner = new Scanner(System.in);
        Product p1 = new Product();
        System.out.println("nhap id");
        p1.id = scanner.nextLong();
        System.out.println("nhap name");
        p1.name = scanner.next();
        System.out.println("nhap price");
        p1.price = scanner.nextDouble();
        cart.productList.add(p1);
        return p1;
    }

    public static Product inputProduct() {
        Scanner scanner = new Scanner(System.in);
        Product p1 = new Product();
        System.out.println("nhap id");
        p1.id = scanner.nextLong();
        System.out.println("nhap name");
        p1.name = scanner.next();
        System.out.println("nhap price");
        p1.price = scanner.nextDouble();
        return p1;
    }

    public static void printAll(Cart cart) {
        for (Product p : cart.productList) {
            System.out.println(p.toString());
        }
    }

    public static void sum(Cart cart) {
        double tong = 0d;
        for (Product p : cart.productList) {
            tong += p.price;
        }
        System.out.println("Tong " + tong);
    }

    public static void addToCart(Product p1, Cart cart1) {
        cart1.productList.add(p1);
    }

    public static void inputManyProduct(Cart cart) {
        System.out.println("Nhap thong tin san pham");
        Scanner scanner = new Scanner(System.in);
        while(true) {
            Product product = ProductManager.inputProduct(cart);
            System.out.println("Ban co muon tiep tuc khong (y/n)?:");
            String s = scanner.next();
            if ("n".equalsIgnoreCase(s)) {
                break;
            }
        }
    }



    public static CartManager cart;

    public static void inputManyCart(List<Cart> cartList) {
        cart.inputManyCart(cartList);
    }
}
