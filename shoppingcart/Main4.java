package shoppingcart;

import java.util.Scanner;

public class Main4 { // nhập thông tin vào 2 cart cố định
    public static void main(String[] args) {
        Cart cart1 = new Cart();
        Cart cart2 = new Cart();

        ProductManager.inputManyProduct(cart1);
        ProductManager.inputManyProduct(cart2);

        System.out.println("Thong tin cart1");
        ProductManager.printAll(cart1);
        ProductManager.sum(cart1);
        System.out.println("\n");

        System.out.println("Thong tin cart2");
        ProductManager.printAll(cart2);
        ProductManager.sum(cart2);
    }
}
