package shoppingcart;

public class Product {
    Long id;
    String name;
    Double price;

    public Product() {

    }

    public Product(long l, String name, double p) {
        this.id = l;
        this.name = name;
        this.price = p;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
