package dec28;

public class InterestCalculator {
    public static void main(String[] args) {
        LaiSuat laiSuat = new LaiSuat();
        laiSuat.setLs1Ngay(2.3);
        laiSuat.setLs1Thang(4.5);
        laiSuat.setLs1Nam(6.9);

        User u1 = new User();
        u1.setId(1);
        u1.setName("Trung");
        UserAccount ua1 = new UserAccount();
        ua1.setId(1);
        ua1.setLoaiLaiSuat("1thang");
        ua1.setTongSoTien(100000);

        User u2 = new User();
        u2.setId(2);
        u2.setName("Hoa");
        UserAccount ua2 = new UserAccount();
        ua2.setId(2);
        ua2.setLoaiLaiSuat("1nam");
        ua2.setTongSoTien(50000);

        tinhLaiSuat(laiSuat, u1, ua1);
        tinhLaiSuat(laiSuat, u2, ua2);
    }

    private static void tinhLaiSuat(LaiSuat laiSuat, User user,
                                    UserAccount userAccount) {
        double interest = 0;
        System.out.println("Lai cua user " + user.getName() +
                " (" + user.getId() + "):");
        if (userAccount.getLoaiLaiSuat().equals("1ngay")) {
            interest = userAccount.getTongSoTien() * laiSuat.getLs1Ngay();
        } else if (userAccount.getLoaiLaiSuat().equals("1thang")) {
            interest = userAccount.getTongSoTien() * laiSuat.getLs1Thang();
        } else if (userAccount.getLoaiLaiSuat().equals("1nam")) {
            interest = userAccount.getTongSoTien() * laiSuat.getLs1Nam();
        }
        System.out.println("= " + interest);
    }
}
