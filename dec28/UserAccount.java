package dec28;

public class UserAccount {
    long id;
    double tongSoTien;
    String loaiLaiSuat; // 1ngay, 1thang, 1nam

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTongSoTien() {
        return tongSoTien;
    }

    public void setTongSoTien(double tongSoTien) {
        this.tongSoTien = tongSoTien;
    }

    public String getLoaiLaiSuat() {
        return loaiLaiSuat;
    }

    public void setLoaiLaiSuat(String loaiLaiSuat) {
        this.loaiLaiSuat = loaiLaiSuat;
    }
}
