package dec28;

public class LaiSuat {
    long id;
    double ls1Ngay;
    double ls1Thang;
    double ls1Nam;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLs1Ngay() {
        return ls1Ngay;
    }

    public void setLs1Ngay(double ls1Ngay) {
        this.ls1Ngay = ls1Ngay;
    }

    public double getLs1Thang() {
        return ls1Thang;
    }

    public void setLs1Thang(double ls1Thang) {
        this.ls1Thang = ls1Thang;
    }

    public double getLs1Nam() {
        return ls1Nam;
    }

    public void setLs1Nam(double ls1Nam) {
        this.ls1Nam = ls1Nam;
    }
}
