package javacore;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WAInput {
    public void input(String s) {
        try {
            File myObj = new File(s);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
                // add du lieu vao weather
                Weather w1 = new Weather();
                w1.temp = 123d;
                w1.la = 12d;
                // ...
                WeatherMainAuto.weatherList.add(w1);
//                WeatherMainAuto.weatherList.add(w2);
//                WeatherMainAuto.weatherList.add(w3);
//                WeatherMainAuto.weatherList.add(w4);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
