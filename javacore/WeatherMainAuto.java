package javacore;

import java.util.ArrayList;
import java.util.List;

public class WeatherMainAuto {
    public static List<Weather> weatherList = new ArrayList<>();

    public static void main(String[] args) {
        String filePath = "C:\\data\\fsoft\\lfmall\\2020-11-13-estimation-pweb-vuejs\\lfmall_pweb_vuejs_estimation_20201113 - Copy.xlsx";
        WAInput waInput = new WAInput();
        waInput.input(filePath);
        WAProcess waProcess = new WAProcess();
        waProcess.process();
        WAOutput waOutput = new WAOutput();
        waOutput.output(filePath);
    }
}
