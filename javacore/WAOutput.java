package javacore;

import java.io.FileWriter;
import java.io.IOException;

public class WAOutput {
    public void output(String s) {
        try {
            FileWriter myWriter = new FileWriter(s);
            myWriter.write(WeatherMainAuto.weatherList.toString());
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
