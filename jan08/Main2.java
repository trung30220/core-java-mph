package jan08;

public class Main2 {
    public static void main(String[] args) {
        int w = 20, h = 11;
        char[][] listChar = new char[w][h];
        int size = 10;
        int t1 = 1;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (j < h/2) {
                    listChar[i][j] = (((j-1)*2+1)<=i)? ' ': '*';
                }
            }
        }

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                System.out.print(listChar[i][j]);
            }
            System.out.println();
        }

    }
}
