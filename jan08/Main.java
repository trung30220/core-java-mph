package jan08;

public class Main {
    public static void main(String[] args) {
//        char[][] board = new char[3][3];
//        Pixel[][] image = new Pixel[1024][768];
        int w = 20, h = 5;
        char[][] listChar = new char[w][h];

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
//                if (j % 2 == 0) {
//                    if (i % 2 == 0) {
//                        listChar[i][j] = '*';
//                    } else {
//                        listChar[i][j] = ' ';
//                    }
//                } else {
//                    if (i%2==0) {
//                        listChar[i][j] = ' ';
//                    } else {
//                        listChar[i][j] = '*';
//                    }
//                }

                listChar[i][j] = (j % 2 == 0) ? ((i % 2 == 0)?'*':' ') : ((i%2==0)?' ':'*');
            }
        }

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                System.out.print(listChar[i][j]);
            }
            System.out.println();
        }

    }
}
