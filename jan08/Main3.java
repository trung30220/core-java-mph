package jan08;

public class Main3 {
    public static void main(String[] args) {
        int w = 20, h = 11;
        char[][] listChar = new char[w][h];
        int size = 10;

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                if (i <= j) {
                    listChar[i][j] = '*';
                }
            }
        }

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                System.out.print(listChar[i][j]);
            }
            System.out.println();
        }
    }
}
