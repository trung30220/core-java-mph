package dec21;

public interface IEmployee {
    public long getId();

    public void setId(long id);

    public String getName();

    public void setName(String name);

    public String getAddress();

    public void setAddress(String address);

    public boolean isChecked();

    public void setChecked(boolean checked);
    public void work();
    public void logIn();
    public void logOut();
    public void offWork();
    public void getSalary();
}
