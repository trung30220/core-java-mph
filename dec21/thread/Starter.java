package dec21.thread;

public class Starter {
    public static void main(String[] args) {
//        new Thread(new DemGaoBac()).start();
//        new Thread(new DemGaoTrung()).start();
//        new Thread(new DemGaoNam()).start();

        DemGaoBac d1 = new DemGaoBac();
        Thread t1 = new Thread(d1);
        t1.start();

        DemGaoTrung d2 = new DemGaoTrung();
        Thread t2 = new Thread(d2);
        t2.start();

        DemGaoNam d3 = new DemGaoNam();
        Thread t3 = new Thread(d3);
        t3.start();
    }
}
