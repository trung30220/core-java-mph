package dec21;

import java.util.ArrayList;
import java.util.List;

public class Main1 {
    public static void main(String[] args) {
        IEmployee e1 = new Employee();
        e1.setId(1);
        e1.setName("Trung");

        IEmployee e2 = new EmployeeLevel2();
        e2.setId(1);
        e2.setName("Trung");

        System.out.println(e1.getName());
        System.out.println(e2.getName());

        List<IEmployee> list = new ArrayList<>();
        list.add(e1);
        list.add(e2);

        for (IEmployee ie : list) {
            System.out.println(ie.getName());
        }
    }
}
