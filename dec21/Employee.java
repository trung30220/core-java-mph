package dec21;

public class Employee implements IEmployee {
    long id;
    String name;
    String address;
    boolean checked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void work() {

    }
    public void logIn() {
        checked = true;
    }
    public void logOut() {
        checked = false;
    }
    public void offWork() {

    }
    public void getSalary() {

    }
}
