package jan04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Board board = new Board();

        board.printBoard();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("First user(x), input x:");
            int x1 = scanner.nextInt();
            System.out.println("First user(x), input y:");
            int y1 = scanner.nextInt();
            System.out.println("Second user(o), input x:");
            int x2 = scanner.nextInt();
            System.out.println("Second user(o), input y:");
            int y2 = scanner.nextInt();
            board.mainBoard[x1][y1] = 'x';
            board.mainBoard[x2][y2] = 'o';

            board.printBoard();
            if (board.checkWinnerHorizontal()) break;
            if (board.checkWinnerVertical()) break;
            if (board.checkWinnerX()) break;
            if (board.checkBoard()) break;
        }
    }
}
