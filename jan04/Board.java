package jan04;

public class Board {
    char[][] mainBoard = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
    };

    public boolean check(String result) {
        if (result.equals("xxx")) {
            System.out.println("x is winner!");
            return true;
        } else if (result.equals("ooo")) {
            System.out.println("o is winner!");
            return true;
        }
        return false;
    }

    public boolean checkWinnerX() {
        String v1 = "" + mainBoard[0][0] + mainBoard[1][1] + mainBoard[2][2];
        String v2 = "" + mainBoard[0][2] + mainBoard[1][1] + mainBoard[2][0];
        if (check(v1)) return check(v1);
        if (check(v2)) return check(v2);
        return false;
    }
    public boolean checkWinnerVertical() {
        String h1 = "" + mainBoard[0][0] + mainBoard[1][0] + mainBoard[2][0];
        String h2 = "" + mainBoard[0][1] + mainBoard[1][1] + mainBoard[2][1];
        String h3 = "" + mainBoard[0][2] + mainBoard[1][2] + mainBoard[2][2];
        if (check(h1)) return check(h1);
        if (check(h2)) return check(h2);
        if (check(h3)) return check(h3);
        return false;
    }
    public boolean checkWinnerHorizontal() {
        String h1 = "" + mainBoard[0][0] + mainBoard[0][1] + mainBoard[0][2];
        String h2 = "" + mainBoard[1][0] + mainBoard[1][1] + mainBoard[1][2];
        String h3 = "" + mainBoard[2][0] + mainBoard[2][1] + mainBoard[2][2];
        if (check(h1)) return check(h1);
        if (check(h2)) return check(h2);
        if (check(h3)) return check(h3);
        return false;
    }

    public boolean checkBoard() {
        for (int i = 0;i < 3;i++) {
            for (int j = 0;j < 3;j++) {
                char c = mainBoard[i][j];
                if (c == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public void printBoard() {
        System.out.println("This is main board:");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(mainBoard[i][j] + " ");
            }
            System.out.println();
        }
    }
}
